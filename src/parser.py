import argparse
from pathlib import Path


def parser():
    """
    This function checks the script parameters and
    returns them for the use in the script.
    returns:
        - args, namespace object with arguments
    """
    parser = argparse.ArgumentParser(
        prog="plotter.py",
        description="""This program plots the data from
        .sNp files and has the ability to save the figure
        as a .pdf file.""",
    )
    parser.add_argument("path_snp", help="Str, path to data file")
    parser.add_argument(
        "-t",
        "--type",
        help="""Str, Type of data to plot
        (mag for magnitude in dB / phase for phase in deg).
        If it is not specified the program will plot mag.
        """,
    )
    parser.add_argument(
        "-p",
        "--plot_data",
        nargs="*",
        help="""Multiple str, formatted a_b c_d ... y_z that
        specifies what traces will be plotted. E.g. for plotting
        S11, S22, and S21 put 1_1 2_2 2_1. If the parameter
        is not specified all traces from .sNp file will be plotted.
        """,
    )
    parser.add_argument(
        "-u",
        "--unit",
        help="""Str, Unit of frequency, by default GHz
        """,
    )
    parser.add_argument(
        "-x",
        "--xlim",
        nargs=3,
        type=float,
        help="""2 float 1 int, X axis limits and number of ticks in format
        xl xu t. E.g. limits from 5 to 10 and 5 ticks the string
        should be 5 10 5.
        """,
    )
    parser.add_argument(
        "-y",
        "--ylim",
        nargs=3,
        type=float,
        help="""2 float 1 int, Y axis limits and number of ticks in format
        yl yu t. E.g. limits from 5 to 10 and 5 ticks the string
        should be 5 10 5.
        """,
    )
    parser.add_argument(
        "-s",
        "--save_path",
        help="""Str, path to save the figure as .pdf file.
        """,
    )
    parser.add_argument(
        "--show",
        nargs="?",
        const=True,
        help="""The figure is beeing shown and script waits for the user
        to close the figure window
        """
    )
    args = parser.parse_args()
    args = check_args(args)
    return args


def check_args(args: argparse.Namespace):
    args.path_snp = str(Path(args.path_snp).resolve())
    if args.type is None:
        args.type = "mag"
    if args.unit is None:
        args.unit = "GHz"
    if args.plot_data is not None:
        args.plot_data = which_plots(args.plot_data)
    if args.xlim is None:
        args.xlim = [None, None, None]
    else:
        args.xlim = convert_tick_number(args.xlim)
    if args.ylim is None:
        args.ylim = [None, None, None]
    else:
        args.ylim = convert_tick_number(args.ylim)
    if args.save_path is not None:
        args.save_path = str(Path(args.save_path).resolve())
    return args


def which_plots(trace_string_list):
    """
    This function changes the list of strings
    ["a_b", "c_d", ..., "y_z"] into list of ints
    [[int(a), int(b)],...,[int(y), int(z)]].
    args:
        - trace_string_list
    returns:
        - trace_list
    """
    trace_list = [x.split("_") for x in trace_string_list]
    for n in range(len(trace_list)):
        for k in range(2):
            trace_list[n][k] = int(trace_list[n][k])
    return trace_list


def convert_tick_number(limits):
    """This function converts tick_number to int in limits list
    [lower_limit, upper_limit, tick_number]
    """
    limits[2] = int(limits[2])
    return limits
