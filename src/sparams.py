import skrf as rf


def read_sparameters_file(filepath: str):
    """
    This function reads .sNp file and returns the network object.
    args:
        - filepath: str     - path to the .sNp file
    returns:
        - dataNetwork: Network - Network object with data from .sNP file
    """
    data_network = rf.Network(filepath)
    return data_network


def get_sparams_mag_dB(data: rf.network.Network):
    """
    This function extracts magnitude of S-parameters
    in dB from the Network object.
    args:
        - data: rf.netwotk.Network - network object with data
    returns:
        - sdb : np.ndarray - numpy array with numerical data
                    of S-parameters
    """
    sdb = data.s_db
    return sdb


def get_sparams_phase_deg(data: rf.network.Network):
    """
    This function extracts phase of S-parameters
    in dB from the Network object.
    args:
        - data: rf.netwotk.Network - network object with data
    returns:
        - phase : np.ndarray - numpy array with numerical data
                    of S-parameters
    """
    phase = data.s_deg
    return phase


def get_desired_data(data: rf.network.Network, type: str):
    """
    This function gets the desired data from the Network object
    args:
        - data: rf.netwotk.Network - network object with data
        - type: str, "mag" or "phase" to indicate desired data.
    returns:
        - return_data : np.ndarray - numpy array with numerical
                data of S-parameters
    """
    if type == "mag":
        return get_sparams_mag_dB(data)
    else:
        return get_sparams_phase_deg(data)


def get_frequency(data: rf.network.Network, freq_unit: str = "GHz"):
    """
    This function extracts frequency data form the Network object and returns
    a frequency vector with the appropriate unit (by default in GHz).
    args:
        - data: rf.netwotk.Network - network object with data
    returns:
        - freq : np.ndarray - numpy array with frequencies
    """
    data.frequency.unit = freq_unit
    freq = data.frequency.f_scaled
    return freq
