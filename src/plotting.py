import matplotlib.pyplot as plt
import numpy as np


def create_plot():
    """
    This function creates and returns new fig and ax objects.
    """
    fig, ax = plt.subplots()
    return fig, ax


def plot_all(ax, freq, data, type):
    """
    This function plots magnitude in dB of all S-parameters.
    It also sets the legend for the plot.
    args:
        - ax: ax object - object to plot the desired traces.
        - freq: np.ndarray - scaled frequency.
        - data: np.ndarray - maginitudes in dB of all S-parameters.
        - type: str - type of data to be plotted "mag" or "phase"
    """
    plot_names = []
    for n in range(data.shape[1]):
        for k in range(data.shape[2]):
            ax.plot(freq, data[:, n, k])
            if type == "mag":
                plot_names.append("S{}{}".format(n + 1, k + 1))
            else:
                # \u2221 is the character for angle sign.
                plot_names.append(
                    "\u2221 S{}{}".format(n + 1, k + 1))
    set_legend(ax, plot_names)


def plot_selected(ax, freq, data, type, selection):
    """
    This function plots magnitude in dB of all S-parameters.
    It also sets the legend for the plot.
    args:
        - ax: ax object - object to plot the desired traces.
        - freq: np.ndarray - scaled frequency.
        - data: np.ndarray - data to be plotted.
        - type: str - type of data to be plotted "mag" or "phase"
        - selection: list - list of selected traces.
    """
    plot_names = []
    for n in range(len(selection)):
        ax.plot(freq, data[:, selection[n][0] - 1, selection[n][1] - 1])
        if type == "mag":
            plot_names.append("S{}{}".format(selection[n][0], selection[n][1]))
        else:
            # \u2221 is the character for angle sign.
            plot_names.append(
                "\u2221 S{}{}".format(selection[n][0], selection[n][1]))
    set_legend(ax, plot_names)


def set_labels(ax, ytype, xlabel_unit="GHz"):
    """
    This function sets labels for the S-parameters plots
    args:
        - ax: ax object - object to plot the desired traces.
        - ytype: str - Type of data to be plotted ("mag" or "phase").
        - xlabel_unit: str - Unit of frequency e.g. ("kHz", "MHz", "GHz").
                       default value "GHz".
    """
    xlabel = "Frequency ({})".format(xlabel_unit)
    ax.set_xlabel(xlabel)
    if ytype == "mag":
        ax.set_ylabel("Magintude of S-parameters (dB)")
    else:
        ax.set_ylabel("Phase of S-parameters (\u00b0)")


def set_limits(
    ax, xl_lim=None, xu_lim=None, yl_lim=None, yu_lim=None, xticks=5, yticks=5
):
    """
    This function sets the limits and tick number on the plot.
    The function adds grid to the plot.
    args:
        - ax: ax object - object to plot the desired traces
        - xl_lim: int, float, double - lower limit of x axis.
        - xu_lim: int, float, double - upper limit of x axis.
        - yl_lim: int, float, double - lower limit of y axis.
        - yu_lim: int, float, double - upper limit of y axis.
        - xticks: int - number of ticks on x axis (Default: 5).
        - yticks: int - number of ticks on y axis (Default: 5).
    """
    if xl_lim is not None:
        ax.set_xlim(xl_lim, xu_lim)
        ax.set_xticks(np.linspace(xl_lim, xu_lim, xticks))
    if yl_lim is not None:
        ax.set_ylim(yl_lim, yu_lim)
        ax.set_yticks(np.linspace(yl_lim, yu_lim, yticks))
    ax.grid()


def set_legend(ax, plot_names):
    """
    This function creates the legend for the plot.
    args:
        - ax: ax object - object to plot the desired traces.
    """
    ax.legend(plot_names, framealpha=1)


def show_plot():
    """
    This function shows the plot.
    """
    plt.show(block=True)


def save_figure(fig, filepath):
    """
    This function saves the figure fig as a pdf file
    given in the filepath.
    args:
        - fig: fig object - figure to save.
        - filepath: str - path to the future pdf file.
    """
    fig.savefig(filepath, format="pdf", bbox_inches="tight")
