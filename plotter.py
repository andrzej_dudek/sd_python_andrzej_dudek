# local import
from src import sparams as vna
from src import plotting as plot
from src import parser


def main():
    args = parser.parser()
    print(args)
    data = vna.read_sparameters_file(args.path_snp)
    freq = vna.get_frequency(data, args.unit)
    fig, ax = plot.create_plot()
    data_to_plot = vna.get_desired_data(data, args.type)
    plot.set_labels(ax, args.type, args.unit)
    if args.plot_data is None:
        plot.plot_all(ax, freq, data_to_plot, args.type)
    else:
        plot.plot_selected(ax, freq, data_to_plot, args.type, args.plot_data)
    plot.set_limits(
        ax,
        args.xlim[0],
        args.xlim[1],
        args.ylim[0],
        args.ylim[1],
        args.xlim[2],
        args.ylim[2],
    )
    if args.show is not None:
        plot.show_plot()
    if args.save_path is not None:
        plot.save_figure(fig, args.save_path)


if __name__ == "__main__":
    main()
