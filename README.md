# sd_python_andrzej_dudek
This is a python project for a python course in Doctoral School.
This project contains the following files:
- plotter.py - main project script.
- src/parser.py - module for parsing the arguments given to the main script when running the script.
- src/sparams.py - module for file and data handling of .sNp files.
- src/plotting.py - module for creation and saving of figures.
- requirements.txt - file with required modules that need to be installed to make the script work.


## Usage
To work with script use comand line and type the command
**python plotter.py** with script arguments.
The detailed help for the use of the script can be found by running 
**python plotter.py -h** or **python.py --help** and is also given below.

## Script help and arguments 
This program plots the data from .sNp files and has the ability to save the figure as a .pdf file.

### positional arguments:
- **path_snp**              Str, path to data file

### options:
 - **-h, --help** - show this help message and exit
 - **-t TYPE, --type TYPE** - Str, Type of data to plot (mag for magnitude in dB / phase for phase in deg). If it is not specified the program will plot mag.
 - **-p [PLOT_DATA ...], --plot_data [PLOT_DATA ...]** - Multiple str, formatted a_b c_d ... y_z that specifies what traces will be plotted. E.g. for plotting S11, S22, and S21 put 1_1 2_2 2_1. If the parameter is not specified all traces from .sNp file will be plotted.
- **-u UNIT, --unit UNIT** - Str, Unit of frequency, by default GHz
- **-x XLIM XLIM XLIM, --xlim XLIM XLIM XLIM** - 2 float 1 int, X axis limits and number of ticks in format xl xu t. E.g. limits from 5 to 10 and 5 ticks the string should be 5 10 5.
-  **-y YLIM YLIM YLIM, --ylim YLIM YLIM YLIM** - 2 float 1 int, Y axis limits and number of ticks in format yl yu t. E.g. limits from 5 to 10 and 5 ticks the string should be 5 10 5.
-  **-s SAVE_PATH, --save_path SAVE_PATH** - Str, path to save the figure as .pdf file.
-  **--show** - The figure is beeing shown and script waits for the user to close the figure window